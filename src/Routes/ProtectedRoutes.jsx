// Packages
import React, { Fragment } from "react"
import { Route, Redirect } from "react-router-dom"
import PropTypes from "prop-types"
const ProtectedRoute = ({ component: Component, ...rest }) => {
  const checkValidToken = () => {
    const token = localStorage.getItem("Token")
    return token;
  };

  return (
    <Fragment>
      {checkValidToken() ? (
        <Route
          {...rest}
          render={(props) => <Component {...rest} {...props} />}
        />
      ) : (
        <Redirect to="/login" />
      )}
    </Fragment>
  )
}

export default ProtectedRoute;

ProtectedRoute.propTypes = {
  component: PropTypes.string.isRequired,
}
