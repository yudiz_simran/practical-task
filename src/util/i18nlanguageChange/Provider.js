import React, { Fragment, fragment } from 'react';
import { IntlProvider } from 'react-intl'

// Components
import { LANG } from './Languages'
import messages from './Messages'

const Provider = ({children, lang = LANG.ENGLISH}) => (
    <IntlProvider
      lang={LANG}
      textComponent={Fragment}
      messages={messages[lang]}
    >
    {children}
    </IntlProvider>
)


export default Provider;
