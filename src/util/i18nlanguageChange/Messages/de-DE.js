import { LANG } from "../languages";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    [LANG.GERMAN]:{
        // Signup-Form
        'Signup':'Anmelden',
        'Please fill this form to Signup!':'Bitte füllen Sie dieses Formular aus, um sich anzumelden!',
        'Username...':'Nutzername...',
        'Email...':'Email...',
        'Password...':'Passwort...',
        'Already have an account Login here':'Hast du schon einen Account? Log dich hier ein',
        'Sign Up':'Anmeldung',
        'Username is required':'Ein Benutzername ist erforderlich',
        'This field cannot contain only blankspaces':'Dieses Feld darf nicht nur Leerzeichen enthalten',
        'Email is required':'E-Mail ist erforderlich',
        'Password is required':'Passwort wird benötigt',
        'User registered successfully':'Benutzer erfolgreich registriert',

        // Login-Form
        'Login':'Anmeldung',
        'Please fill this form to Login!':'Bitte füllen Sie dieses Formular aus, um sich anzumelden!',
        'Email...':'Email...',
        'Password...':'Passwort...',
        'Do not have an account Signup here':'Sie haben kein Konto Registrieren Sie sich hier',
        'Login succesfully':'Erfolgreich einloggen',
        'Invalid user':'Ungültiger Benutzer',

        // Home-Page
        'Logout':'Ausloggen',
        'Search by name':'Suche mit Name',
        'Export to CSV':'In CSV exportieren',
        'Name':'Name',
        'Prize':'Preis',
        'Actions':'Aktionen',

        // Add-Product
        'Add Product':'Produkt hinzufügen',
        'Product Name...':'Produktname...',
        'Prize...':'Preis...',
        'Product name is required':'Produktname ist erforderlich',
        'Prize is required':'Preis ist erforderlich',
        'Add':'Hinzufügen',
        'Cancel':'Abbrechen',

        // Edit-Product
        'Edit Product':'Produkt bearbeiten',
        'Edit':'Bearbeiten',
        'Product edited successfully':'Produkt erfolgreich bearbeitet',

        // View-Product
        'Go back to Home':'Geh zurück nach hause',

        // Delete-Product
        'Product deleted successfully':'Produkt erfolgreich gelöscht',


    }
}
