import { LANG } from "../languages";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    [LANG.FRENCH]:{
        // Signup-Form
        'Signup':'Inscrivez-vous',
        'Please fill this form to Signup!':'Veuillez remplir ce formulaire pour vous inscrire!',
        'Username...':'Nom d utilisateur...',
        'Email...':'E-mail...',
        'Password...': 'Mot de passe...',
        'Already have an account Login here':'Vous avez déjà un compte? Connectez-vous ici',
        'Sign Up':'Sinscrire',
        'Username is required':'Nom dutilisateur est nécessaire',
        'This field cannot contain only blankspaces':'Ce champ ne peut pas contenir que des espaces',
        'Email is required':'Le-mail est requis',
        'Password is required':'Mot de passe requis',
        'User registered successfully':'Utilisateur enregistré avec succès',

        // Login-Form
        'Login':'Connexion',
        'Please fill this form to Login!':'Veuillez remplir ce formulaire pour vous connecter!',
        'Email...':'E-mail...',
        'Password...': 'Mot de passe...',
        'Do not have an account Signup here':'Vous navez pas de compte Inscrivez-vous ici',
        'Login succesfully':'Connectez-vous avec succès',
        'Invalid user':'Utilisateur invalide',

        // Home-Page
        'Logout':'Se déconnecter',
        'Search by name':'Recherche par nom',
        'Export to CSV':'Exporter au format CSV',
        'Name':'Nom',
        'Prize':'Prix',
        'Actions':'Actions',

        // Add-Product
        'Add Product': 'Ajouter un produit',
        'Product Name...':'Nom du produit...',
        'Prize...':'Prix...',
        'Product name is required':'Le nom du produit est requis',
        'Prize is required':'Le prix est requis',
        'Add':'Ajouter',
        'Cancel':'Annuler',

        // Edit-Product
        'Edit Product':'Modifier le produit',
        'Edit':'Éditer',
        'Product edited successfully':'Produit modifié avec succès',

        // View-Product
        'Go back to Home':'Retournez à laccueil',

        // Delete-Product
        'Product deleted successfully': 'Produit supprimé avec succès',
    }
}
