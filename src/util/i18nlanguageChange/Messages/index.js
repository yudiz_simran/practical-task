import en from './en-US';
import de from './de-DE';
import fr from './fr-CA';
import ko from './ko-SK';

export default {
    ...en,
    ...de,
    ...fr,
    ...ko
}
