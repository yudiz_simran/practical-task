import { LANG } from "../languages";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    [LANG.Korian]:{
        // Signup-Form
        'Signup':'가입하기',
        'Please fill this form to Signup!':'이 양식을 작성하여 가입하십시오!',
        'Username...':'사용자 이름...',
        'Email...':'이메일...',
        'Password...': '비밀번호...',
        'Already have an account Login here':'이미 계정이 있습니다 여기에서 로그인하십시오',
        'Sign Up':'가입하기',
        'Username is required':'사용자 이름은 필수 항목입니다.',
        'This field cannot contain only blankspaces':'이 필드에는 공백만 포함될 수 없습니다.',
        'Email is required':'이메일이 필요합니다',
        'Password is required':'비밀번호가 필요합니다',
        'User registered successfully':'사용자가 성공적으로 등록되었습니다.',

        // Login-Form
        'Login':'로그인',
        'Please fill this form to Login!':'이 양식을 작성하여 로그인하십시오!',
        'Email...':'이메일...',
        'Password...': '비밀번호...',
        'Do not have an account Signup here':'계정이 없습니다 여기에서 가입하세요',
        'Login succesfully':'로그인 성공',
        'Invalid user':'잘못된 사용자',

        // Home-Page
        'Logout':'로그 아웃',
        'Search by name':'이름으로 검색',
        'Export to CSV':'CSV로 내보내기',
        'Name':'이름',
        'Prize':'상',
        'Actions':'행위',

        // Add-Product
        'Add Product': '제품 추가',
        'Product Name...':'상품명...',
        'Prize...':'상...',
        'Product name is required':'제품 이름은 필수 항목입니다.',
        'Prize is required':'상이 필요합니다',
        'Add':'추가하다',
        'Cancel':'취소',

        // Edit-Product
        'Edit Product':'제품 수정',
        'Edit':'편집하다',
        'Product edited successfully':'제품이 성공적으로 수정되었습니다.',

        // View-Product
        'Go back to Home':'홈으로 돌아가기',

        // Delete-Product
        'Product deleted successfully': '제품이 성공적으로 삭제되었습니다.',
    }
}
