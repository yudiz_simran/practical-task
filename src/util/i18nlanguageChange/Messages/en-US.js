import { LANG } from "../languages";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    [LANG.ENGLISH]:{
        // Signup-Form
        'Signup':'Signup',
        'Please fill this form to Signup!':'Please fill this form to Signup!',
        'Username...':'Username...',
        'Email...':'Email...',
        'Password...': 'Password...',
        'Already have an account Login here':'Already have an account Login here',
        'Sign Up':'Sign Up',
        'Username is required':'Username is required',
        'This field cannot contain only blankspaces':'This field cannot contain only blankspaces',
        'Email is required':'Email is required',
        'Password is required':'Password is required',
        'User registered successfully':'User registered successfully',

        // Login-Form
        'Login':'Login',
        'Please fill this form to Login!':'Please fill this form to Login!',
        'Email...':'Email...',
        'Password...':'Password...',
        'Do not have an account Signup here':'Do not have an account Signup here',
        'Login succesfully':'Login succesfully',
        'Invalid user':'Invalid user',

        // Home-Page
        'Logout':'Logout',
        'Search by name':'Search by name',
        'Export to CSV':'Export to CSV',
        'Name':'Name',
        'Prize':'Prize',
        'Actions':'Actions',

        // Add-Product
        'Add Product': 'Add Product',
        'Product Name...':'Product Name...',
        'Prize...':'Prize...',
        'Product name is required':'Product name is required',
        'Prize is required':'Prize is required',
        'Add':'Add',
        'Cancel':'Cancel',

        // Edit-Product
        'Edit Product':'Edit Product',
        'Edit':'Edit',
        'Product edited successfully':'Product edited successfully',

        // View-Product
        'Go back to Home':'Go back to Home',

        // Delete-Product
        'Product deleted successfully': 'Product deleted successfully',
    }
}
