import { toast } from 'react-toastify'

export const displayFormErrors = (
  key = '',
  errors = {},
  touched = {},
  submitCount = 1
) => {
  if (errors[key] !== undefined && errors[key] && submitCount) {
    return (
      // eslint-disable-next-line react/react-in-jsx-scope
      <div className='text-danger input-feedback font12'>{errors[key]}</div>
    )
  }
  return null
}

export const apiToastSuccess = (successMsg) => {
  showToast(successMsg, 1)
}

export const apiToastError = (errorMsg) => {
  showToast(errorMsg, 0)
}

export const showToast = (msg, status) => {
  if (status) {
    toast.success(msg)
  } else {
    toast.error(msg)
  }
}
