import React from 'react'
import './App.css'

// Bootstrap library
import 'bootstrap/dist/css/bootstrap.min.css'

// Toastify notification
import 'react-toastify/dist/ReactToastify.css'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import { ToastContainer, Flip } from 'react-toastify'

// Components
import Home from './Components/Home/Home'
import Login from './Components/Login/Login'
import AddEditProduct from './Components/Add-Edit-Products/addEditProducts'
import Signup from './Components/Signup/Signup'
import { GlobalProvider } from './Context/GlobalState'
import ProtectedRoute from './Routes/ProtectedRoutes'
import ViewProduct from './Components/View-Product/viewProduct'

function App () {
  return (
    <>
       <div className="App">
         <GlobalProvider>
          <Router>

              <Switch>
                <ProtectedRoute path="/edit/:id" component={AddEditProduct} />
                <ProtectedRoute path="/add" component={AddEditProduct} />
                <ProtectedRoute path="/view/:id" component={ViewProduct} />
                <ProtectedRoute path="/home" component={Home} />
                <Route exact path="/login" component={Login}></Route>
                <Route exact path="/" component={Signup}></Route>
              </Switch>
            </Router>
            {/* <Routes /> */}
            {/* <Routes /> */}
         </GlobalProvider>
      {/* Toastify notifications */}
         <ToastContainer
              position="top-right"
              transition={Flip}
              autoClose={3000}
              hideProgressBar={true}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              // pauseOnFocusLoss
              draggable
              pauseOnHover
         />
       </div>
    </>
  )
}

export default App
