import React, { useContext, useEffect, useState } from 'react'
import {
  Link
} from 'react-router-dom'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import PropTypes from 'prop-types'
// Compponents
import Header from '../Header/header'
import { GlobalContext } from '../../Context/GlobalState'

const ViewProduct = (props) => {
  const [user, setUser] = useState([])
  const { users } = useContext(GlobalContext)
  // To get specific ID
  const getId = () => {
    const { match } = props
    return match?.params?.id
  }
  useEffect(() => {
    getProduct()
  })

  const getProduct = () => {
    const id = getId()
    const getSpecificUser = users?.find(user => user.id === id)
    setUser(() => getSpecificUser)
  }
  return (
        <>
         <Header />
         <Card className="MainCard" >
          <CardActionArea>
           <img src={user.imageurl} className="ViewUserImage" />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
             {user.pname}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
            {user.prize}
            </Typography>
          </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" color="primary" className="gotoBack">
              <Link to='/home'>Go back to Home</Link>
            </Button>
          </CardActions>
    </Card>
        </>
  )
}

export default ViewProduct

ViewProduct.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired
    })
  })
}
