import React, { useEffect } from 'react'

import Container from '@material-ui/core/Container'
import { apiToastSuccess, apiToastError } from '../../util/util'
import { useHistory, Link } from 'react-router-dom'

import { Formik, Field } from 'formik'
import * as Yup from 'yup'
import { v4 as uuidv4 } from 'uuid'

// Validation Schema for login page
const ValidationSchema = Yup.object().shape({
  email: Yup.string()
    .required('Email is required')
    .matches(/^(\S)/, 'This field cannot contain only blankspaces'),
  password: Yup.string()
    .required('Password is required')
    .matches(/^(\S)/, 'This field cannot contain only blankspaces')
})

export default function Signup (props) {
  const history = useHistory()
  useEffect(() => {})

  // Form submit function
  const onHandleSubmit = (val) => {
    localStorage.setItem('Token', uuidv4())
    // eslint-disable-next-line no-unused-expressions
    if (
      val.email === localStorage.getItem('Email') &&
      val.password === localStorage.getItem('Password')
    ) {
      history.push('/home')
      apiToastSuccess('Login succesfully')
    } else {
      apiToastError('Invalid user')
    }
  }

  return (
    <React.Fragment>
      <Container className="mt-4">
        <Formik
          enableReinitialize
          initialValues={{
            username: '',
            email: '',
            password: ''
          }}
          validationSchema={ValidationSchema}
          onSubmit={onHandleSubmit}
        >
          {({
            errors,
            touched,
            handleSubmit,
          }) => {
            return (
              <>
                <form onSubmit={handleSubmit} id="quickForm">
                  <h2>Login</h2>
                  <p>Please fill this form to Login!</p>
                  <hr />
                  <div className="form-group">
                    <Field
                      name="email"
                      label="Email"
                      autoComplete="given-name"
                      className="form-control"
                      placeholder="Email..."
                    />
                    {errors.email && touched.email
                      ? (
                      <div className="error">{errors.email}</div>
                        )
                      : null}
                  </div>
                  <div className="form-group">
                    <Field
                      name="password"
                      label="Password"
                      autoComplete="given-name"
                      className="form-control"
                      placeholder="Password..."
                      type="password"
                    />
                    {errors.password && touched.password
                      ? (
                      <div className="error">{errors.password}</div>
                        )
                      : null}
                  </div>
                  <span>
                    Do not have an account<Link to="/"> Signup </Link>here
                  </span>
                  <div className="form-group">
                    <button type="submit" className="btn btn-primary btn-lg">
                      Login
                    </button>
                  </div>
                  <span></span>
                </form>
              </>
            )
          }}
        </Formik>
      </Container>
    </React.Fragment>
  )
}
