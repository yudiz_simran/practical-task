import React, { useEffect } from 'react'

import Container from '@material-ui/core/Container'
import { apiToastSuccess } from '../../util/util'
import {
  useHistory,
  Link
} from 'react-router-dom'
import { Formik, Field } from 'formik'
import * as Yup from 'yup'

// Validation Schema for signup page
const ValidationSchema = Yup.object().shape({
  username: Yup.string()
    .required('Username is required')
    .matches(/^(\S)/, 'This field cannot contain only blankspaces'),
  email: Yup.string()
    .required('Email is required')
    .matches(/^(\S)/, 'This field cannot contain only blankspaces'),
  password: Yup.string()
    .required('Password is required')
    .matches(/^(\S)/, 'This field cannot contain only blankspaces')
})

export default function Signup (props) {
  const history = useHistory()

  useEffect(() => {

  })

  // Form subgmit function
  function onHandleSubmit (val) {
    // eslint-disable-next-line no-unused-expressions
    localStorage.setItem('Username', val.username)
    localStorage.setItem('Email', val.email)
    localStorage.setItem('Password', val.password)
    history.push('/login')
    apiToastSuccess('User registered successfully')
  }

  return (
    <React.Fragment>
      <Container className="mt-4">
       <Formik
            enableReinitialize
            initialValues={{
              username: '',
              email: '',
              password: ''
            }}
            validationSchema={ValidationSchema}
            onSubmit={onHandleSubmit}
            >
            {({
              errors,
              touched,
              handleSubmit
            }) => {
              return (
                <>
                  <form onSubmit={handleSubmit} id="quickForm">
                    <h2>Signup</h2>
                    <p>Please fill  this form to Signup!</p>
                     <hr />
                    <div className="form-group">
                     <Field
                        name="username"
                        label="Username"
                        autoComplete="given-name"
                        className="form-control"
                        placeholder="Username..."
                    />
                    {errors.username && touched.username
                      ? (
                        <div className="error">{errors.username}</div>
                        )
                      : null}
                    </div>
                    <div className="form-group">
                      <Field
                        name="email"
                        label="Email"
                        autoComplete="given-name"
                        className="form-control"
                        placeholder="Email..."
                      />
                      {errors.email && touched.email
                        ? (
                          <div className="error">{errors.email}</div>
                          )
                        : null}
                   </div>
                    <div className="form-group">
                       <Field
                          name="password"
                          label="Password"
                          autoComplete="given-name"
                          className="form-control"
                          placeholder="Password..."
                          type="password"
                        />
                    {errors.password && touched.password
                      ? (
                        <div className="error">{errors.password}</div>
                        )
                      : null}
                  </div>
                  <span>Already have an account<Link to="/login"> Login </Link>here</span>
                <div className="form-group">
                    <button type="submit" className="btn btn-primary btn-lg">Sign Up</button>
                </div>
                  <span></span>
                   </form>
                </>
              )
            }}
        </Formik>
      </Container>
    </React.Fragment>
  )
}
