import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";

import PropTypes from "prop-types";
import { v4 as uuid } from "uuid";

import * as Yup from "yup";
import { Formik, Field } from "formik";

// Material UI Components
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import { apiToastSuccess } from "../../util/util";

// Components
import { GlobalContext } from "../../Context/GlobalState";
import Header from "../Header/header";

// Validation Schema
const ValidationSchema = Yup.object().shape({
  pname: Yup.string()
    .required("Product name is required")
    .matches(/^(\S)/, "This field cannot contain only blankspaces"),
  prize: Yup.string()
    .required("Prize is required")
    .matches(/^(\S)/, "This field cannot contain only blankspaces"),
});

export default function AddUser(props) {
  const { users, addUser, editUser } = useContext(GlobalContext);

  const history = useHistory();

  // To get specific ID
  const getId = () => {
    const { match } = props;
    return match?.params?.id;
  };

  // Initial values of formik form
  function initialValues() {
    if (!getId()) {
      return {
        pname: "",
        prize: "",
      };
    } else {
      const id = getId();
      const updatedUser = users?.find((user) => user.id === id);
      return {
        pname: updatedUser?.pname,
        prize: updatedUser?.prize,
      };
    }
  }

  // Form submitting method
  function onHandleSubmit(val) {
    const apiVal = {
      id: uuid(),
      pname: val.pname,
      prize: val.prize,
    };

    const editapiVal = {
      id: getId(),
      pname: val.pname,
      prize: `$` +  val.prize,
    };
    if (getId()) {
      apiToastSuccess("Product edited successfully");
      editUser(editapiVal);
    } else {
      apiToastSuccess("Product added successfully");
      addUser(apiVal);
    }
    history.push("/home");
  }

  return (
    <React.Fragment>
      <Header />
      <Container className="mt-4">
        <Formik
          enableReinitialize
          initialValues={initialValues()}
          validationSchema={ValidationSchema}
          onSubmit={onHandleSubmit}
        >
          {({
            errors,
            touched,
            handleSubmit,
          }) => {
            return (
              <>
                <form onSubmit={handleSubmit} id="quickForm">
                  <Typography variant="h5" gutterBottom>
                    {getId() ? "Edit Product" : "Add Product"}
                  </Typography>
                  <Grid container spacing={3}>
                    {/* Product Name Input */}
                    <Grid item xs={12} sm={6}>
                      <Field
                        name="pname"
                        label="Product Name"
                        autoComplete="given-name"
                        className="form-control"
                        placeholder="Product Name..."
                      />
                      {errors.pname && touched.pname ? (
                        <div className="error">{errors.pname}</div>
                      ) : null}
                    </Grid>

                    {/* Product prize Input */}
                    <Grid item xs={12} sm={6}>
                      <Field
                        name="prize"
                        label="Prize"
                        autoComplete="family-name"
                        className="form-control"
                        placeholder="Prize..."
                      />
                      {errors.prize && touched.prize ? (
                        <div className="error">{errors.prize}</div>
                      ) : null}
                    </Grid>

                    {/* Submit Button */}
                    <Grid item xs={12} sm={12}>
                      <Button
                        variant="contained"
                        color="secondary"
                        type="submit"
                        className="mx-2"
                      >
                        {getId() ? "Edit" : "Add"}
                      </Button>
                      <Button variant="contained" color="danger" type="submit">
                        <Link to="/home" className="textAlign">
                          Cancel
                        </Link>
                      </Button>
                    </Grid>
                  </Grid>
                </form>
              </>
            );
          }}
        </Formik>
      </Container>
    </React.Fragment>
  );
}

AddUser.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }),
  }),
};
