import React from 'react'
import PropTypes from 'prop-types'
const Pagination = ({
  dataPerPage,
  totalData,
  paginate
}) => {
  const pageNumbers = []
  for (let i = 1; i <= Math.ceil(totalData / dataPerPage); i++) {
    pageNumbers.push(i)
  }
  return (
       <>
         <nav>
             <ul className="pagination">
                {
                    pageNumbers.map((number) => {
                      return (
                           <>
                              <li key={number} className="page-item">
                               <a onClick={() => paginate(number)} className="page-link">
                                {number}
                              </a>
                             </li>
                           </>
                      )
                    })
                }
             </ul>
         </nav>
       </>
  )
}

export default Pagination

Pagination.propTypes = {
  dataPerPage: PropTypes.string.isRequired,
  totalData: PropTypes.string.isRequired,
  paginate: PropTypes.string.isRequired

}
