/* eslint-disable no-unused-vars */
import React, { useContext, useState, useEffect } from 'react'

import { Link } from 'react-router-dom'
import { CSVLink } from 'react-csv'

// Material ui Components
import Fab from '@material-ui/core/Fab'

// Icons
import AddIcon from '@material-ui/icons/Add'
import CreateIcon from '@material-ui/icons/Create'
import DeleteIcon from '@material-ui/icons/Delete'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import VisibilityIcon from '@material-ui/icons/Visibility'

// Components
import Header from '../../Components/Header/header'
import {apiToastSuccess} from '../../util/util'
import { GlobalContext } from '../../Context/GlobalState'
import Pagination from '../Pagination/Pagination'

// Header of CSV File
const headers = [
  { label: 'Product Name', key: 'pname' },
  { label: 'Prize', key: 'prize' }
]

function Home () {
  const { users, removeUser } = useContext(GlobalContext)
  const [currentData, setCurrentData] = useState(users)

  // For pagination
  const [currentPage, setCurrentPage] = useState(1)
  const dataPerPage = 10
  const indexOfLastData = currentPage * dataPerPage
  const indexOfFirstData = indexOfLastData - dataPerPage

  // const [setfilterData] = useState(currentData)
  useEffect(() => {
    setCurrentData(() => users.slice(indexOfFirstData, indexOfLastData))
  }, [users])

  // To delete user
  const deleteUser = async (id) => {
        await removeUser(id)
        apiToastSuccess("Product deleted successfully");
  }

  const getValues = () => {
    setCurrentData(() => users.slice(indexOfFirstData, indexOfLastData))
  }

  // To change Page
  const paginate = (pageNumber) => {
    const totalvalue = (dataPerPage * (pageNumber - 1)) - 1 < 0 ? 0 : (dataPerPage * (pageNumber - 1))
    setCurrentPage(pageNumber)
    const data = users.slice(totalvalue, dataPerPage * pageNumber)
    setCurrentData(data)
  }
  // For Sorting
  const [sort, setSorting] = useState('asc')

  // To sort list asc and desc
  const sorted = currentData.sort((a, b) => {
    // eslint-disable-next-line eqeqeq
    const isRevered = (sort == 'asc') ? 1 : -1
    return isRevered * a.pname.localeCompare(b.pname)
  })

  // For searching
  const handleChange = (e) => {
    (async () => {
      try {
        const array = users.filter((data) => data.pname.includes(e.target.value))
        const data = array.slice(indexOfFirstData, indexOfLastData)
        setCurrentData(() => data)
        if (e.target.value === '') {
          getValues()
        }
      } catch (e) {
        console.log(e)
      }
    })()
  }

  // To download CSV File
  const csvReport = {
    data: users,
    headers: headers,
    filename: 'Products.csv'
  }
  return (
     <>
       <Header />
        <div className="container mt-4">
          <div className="topnav col-lg-3">
            <input type="text" placeholder="Search by name" className="form-control" onChange={(e) => handleChange(e)} />
          </div>
          {/* Add Button */}
            <div >
              <Link to="/add" className="addButton"><Fab size="small" color="secondary" aria-label="add"><AddIcon /></Fab></Link>
            </div>
          {/* Export to CSV Button */}
            <button className="btn btn-primary primary-button" >
            <CSVLink {...csvReport} style={{ color: 'white' }}>Export to CSV</CSVLink>
            </button>
           {/* Main Table */}
          <table id="dtBasicExample" className="table table-striped table-bordered table-sm " cellSpacing='0' width="100%">
            <thead>
              <tr>
                <th className='th-sm'>
                   Name
                   <ArrowDownwardIcon onClick={() => setSorting('asc')} />
                   <ArrowUpwardIcon onClick={() => setSorting('desc')} />
                   {/* <ImportExportIcon /> */}
                  </th>
                  <th className='th-sm'>Prize
                    <ArrowDownwardIcon onClick={() => setSorting('asc')} />
                    <ArrowUpwardIcon onClick={() => setSorting('desc')} />
                  </th>
                <th className='th-sm'>Actions</th>
              </tr>
            </thead>
            <tbody>
              {
                currentData.map((data) => {
                  return (
                     <>
                       <tr>
                        <td>{data.pname}</td>
                        <td>{data.prize}</td>
                        <td>
                        <div data-tag="allowRowEvents row">
                            <button className="btn btn-sm btn-primary mx-2 ">
                                <Link to={`/view/${data.id}`}>
                                   <VisibilityIcon className="edit-button"></VisibilityIcon>
                                </Link>
                             </button>
                            <button className="btn btn-sm btn-warning mx-2 ">
                                <Link to={`/edit/${data.id}`}>
                                   <CreateIcon className="edit-button"></CreateIcon>
                                </Link>
                             </button>
                            <button className="btn btn-sm btn-danger"
                               onClick={() => deleteUser(data.id)}
                            >
                                <DeleteIcon></DeleteIcon>
                            </button>
                         </div>
                        </td>
                        </tr>
                     </>
                  )
                })
              }
            </tbody>
          </table>
          {
            users.length > 0 && <Pagination
              dataPerPage={dataPerPage}
              className="pagination"
              totalData={users.length}
              paginate={paginate}
            />
          }
        </div>
      </>
  )
}

export default Home
