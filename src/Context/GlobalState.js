import React, { createContext, useReducer } from 'react'
import AppReducer from './AppReducer'

const initialState = {
  users: [
    {
      id: '1',
      pname: 'Watch',
      prize: '$30',
      imageurl: 'https://images.unsplash.com/photo-1523275335684-37898b6baf30?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cHJvZHVjdHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80'
    },
    {
      id: '2',
      pname: 'Laptop',
      prize: '$45',
      imageurl: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBIUEhIRERQRERISERESEQ4SEhIQEBAQFxgYGBgTFxcbIC4kGx0rIBcXJTYlKTI9MzM4GiQ5PjkxPSwyMzABCwsLEA4QHRISGjIpICkwMjIyMjMwMjIyMjIyMjIwMjI1NDIyMDIyMDIyMjIyMjQyMjIwMDIyMjIyMjIwMjAwMv/AABEIAKgBLAMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAwUBAgQGBwj/xAA9EAACAQIDBQUFBQcEAwAAAAAAAQIDEQQhMQUSQVFhBhMicaEyUoGRsRRCYtHwFiNygpLB4QczQ/FjorP/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIDBAUG/8QANBEAAgIBAgIGCAYDAQAAAAAAAAECAxEhMQRBBRITUXGhMmGBkbHB0fAVIkJi4fEjUnIU/9oADAMBAAIRAxEAPwD4yAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASDJPhY03K1VzhBp+KEVOUXwe62rr4noqPZelVjvUcVF30VSjKC/wDSU/oSot7BavC3PLA9PPsXifuTw1V8Ixq9230XeRic0+yeOSuqEpr/AMU6dZ/BQk2w4tci0q5Q9JNeJQA78VsjE0k3VoV6aWbdSlUgkvOSRxbr/WZBXBqAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADuwGPnTeTe7xXL9cjhMkptPKB7/Z21FJJ3T63+v8Af5HoMNiVLXXrx/P9I+VYPFOnK/Div7n0js1GM4Kpdu6vaS8Furer42R3V3V9XMnqenTxPbQ7G1Z7mWsqk45wk4dYyaV/hp9OBtGpKb3am7UTjnGrGNZWz1308v1yLWnhYvdvGytxd8vP6X9DX7ArvcvFLRa2XTk/ToVjx3DPRyXtwfO8b0TxHW61Xk8FBtTA4ONNueGwt7WuqUKP/wA91/8ATKGns/Z9RNPDum803SrVE81qu838/wBZlj2hw9WUd1JNLVPKyKOhFwd5RkrXtdPJLTPia/4bJYjh+Bz08PxdNObHJP15f1O39j8JOO9GeKpq8Vvy7uvHetpkofpEE+wKavTxUeK/e0ZQu/5JT6Houz+PhGbp1VelVUYzz8MeKknwsy7xOGjSnutuUZxc4ztaLSyaf4tPQ8/jEqputaNrMf3Jekv+o813NPvS9To6fbpdfXXDxyfJ+DW3ryuR82q9hMSs4zw01l7NRwdmr/8AJGCOCr2Px8U33DkkrvcqUqtv6JM+lVKijJpO+7LPyta/VEtKd8uqv62+p434hYt4rzPpPwetrKk/v3HyCtsfEwV6lDEQX46NSP1RwH3hVHHi47zV7NptfDzOqhHvElNRmldNTjGcUtEvEmW/FEvSh7mctnRLisqax4f2fnywP0ZPslgJp72Ewzy1jSjT3uq3LMpto/6dYGcKkaVGVGbjLcqRnWnGE+DcJSd1fgerVmyPWSPGtnCqXVkz4YDt2jgp0ak6VSMoTpycJQkrNSX1XFPk0cQLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAvOze3pYSopW36bfjhaO95xbWT+pRghpNYZMZOLytz7/s/G061OFWlJTpyV1JcHxTTz3uh1wmfDNibexGFnvUZ2i/apSzpz848+qzPd4X/UTCyS7ynWpStnuqM6d+mafofPcV0bZCWYfmXmj26OOrmsTeH5HtZ04SyaT1ydmvM4a2zKcs91X52u/UrMP2xwM9K0V0qRnD1krFlh9q0Kn+3Upz/gqRk/RnIldS86p+1HZHq2LTDXsZXYjZEdU2uWUX6KxYUZt0lSqNNx0ml7L4SjfVJa88+Vzqk4vj6nLiKCed7+Su/mdsukrb61VY84aaeNU1zTM6+jeHjb2qi4yaw8bNetbHmMTiZRqyhUykmrSTvCafFPivUt9l4iDUm9Yxvby0Kzamy5PxJ672Tby5FZh8TVoSvKLkldWi1o+XM6JwjfDR/m+P0+B2yrnVtrD1br+PNes9GsVvTd3xyPQbLg55LJXV310t11Pmc9o91WjKL8MnvWlGSp2v8AHPnb5H0zZe06delGpS3I2teMVFbk/dX5nJxdXZqOV+V8+7+xxF0JxxX/AF9/fLPrKUHFJKLaSta65cTO81923m4/mUc9qVFa6jbm7zb8kiKe0feVujUU/lZ2PQp6RhGKWD56fRcrHnTX2/M8p/qj2c7+m8bRipVKMd2sotPeor72Szcfpfkj43Y/QuIx11bNprTg/nk/kfIO1+w+4qynTVqM25QST8D+9D4arp5HTVxkLZY5mdnR9lMMrZeR5gAHUcYAAAAAAAAAAAAAAAAM2AMA2sZUQTg0sZsSKJJGmCUiBRNlA6oUTppYW5DLKBXKkyWGFb4F5QwC1eS4t6IirY6nDw00qkve+4vjx+HzKOT5GnZpatletnu13klq3kka08Fvu0Lv8TyXwOynCdSSc23yWkV5It4p04rcpTrTdsoxl3cb82ld+StrqZylKPiaRqjLwIsB2cglv1X4bq93ZXei6vosybG4bDbu7RpQTTs5VYTTkuNkmrfF36Iikqk5XqttXvGCUXur3NLR04a65k8Kdsvg7K1uel7cc/Q5puX6n8l/J111wxpH6lZU2bSeSpyvxcKjVpckmnd9NTnlsiDbSlUjbLOCkm+WTvf4F3J5Z8I5X3W1HhF31jn5vlYxOCtwV1Z5OOt27Wskvw+hKukub+PxyWlwtct4r3Y+GH5lRTo4iGdLEuKWWVSpRt0s7HZR2xtSFnGpKquipVk/isyecE21yebdskr5SWqd9I3tloR1aa+8rbudpLRvi1xenJdCrkpelFPxSf0LKiUfRk14Sa+pvLtji4v99Rg7c4zpu/ne3oaS7VU55ToyjytU3svkiGUmmt2U7XSSVR24ZcE3z0Ryzlfe3t2WUrRcYp381G+XyNIVV/6+5s2V3E16qx47nGL89GYxOOoT0c48t5W+lyXY23amFqKdNqpB5TpvwKUOXR8mV1SELJ7qWdnaWfwje/PPQheHTa3W1GTajKSaTtrpc6OzjKPVayu5nBdxdrn1tM96yvfnKZ9o2VtulXpqpSlvR405WU4S4xa5/rM6JbsruDd87wb1k+LvqfFcPgrreVSMGnlfeT81bhlqd0Z42EluV5OVk0lWu2uFoyZ5cujoxk+pPHivns/HRnfTxrcU5VvP7Wnn2ZyvvU+oTk1dXfC99XwzODaFGNanOnOzUlk8rxlwkuqPFVNvbTil3m9JXupSpRuv5opX+JpDtfWXt04S8nKH5kw4Ox6pp+DOpdJcNjFqcfGL+WSk2jhJUqkqc1Zp26ea6PU4y721tWGIUZbjhUjle+8nDk/1zKQ9atycV1lqfOcVGuNj7KWY8gAC5zgAAAAAAAAAzYJGyQJSMGUjKRuogkwokkYGVAnjAnBZI0hTOmnRN6dI6qdMlRLrQUcOYr4+nTyXjn7sdF5sjxteSi4xhJ7yabXBMpL21TQksESsxsdeIxdSplJ2jwgsor4cfib4ekc1OrHid+Gr0+di0UiItN5bLXA09D0uASyPO4SpB6Si+l1cvcH5/AtOlNHZVPGxewUZKzSlwaaTXxNKuzaUtYR/l8HwyNaM8ifvDzLeEedND0YXJ7ldW2JTa8E5wzvlu2vppb11K2p2fnH2KkX0acLfU9DKoaORzdhauZupVPkeTrbMxEV7KlbJOMouyz005nDVVSDTlTlGzvaKlBL+F5201PbyOarEulKO6LquD2bPBSqJtrjZ28MZ+LrfTz1IaiacouykrJWbv/Dk7Ws/Q9jiMNF6xT+COaezYStdPLTxN28kzSN0Y7oxt4aT2Z5BXzir5vxLK0racMks7skwtJNpe1k3a3inP3bJ+KC3b8Pz9YuzcJ3alOEmrXio5R5Wt+rG9PsfNJ93Ug96ye9GULRVsk027v5dOR8XXtk4f/HYpZaKOlBbv8qgnd+N+6m80ldeFZZa6X1qUVaWivNK+7GN5eUckln7Jd1ezGJivYUm3/xzi1GPlNpyf68pMBsCpZTrRcErpUru81e/izslfgjN2x3ydca28RwVGG2ZKrablKMU/FNSlep0WnzJ8Tg4paX88382X9eNlbS2i0seY27tNQvCGdTTmqf+ehSM52yOrFPDVty2KTacYRdko734b2RXqLLXC7KlLx1L552er6smrYVLgd6sUfy7nhXVu6Tsa6udkUTRg7qtA5pUzVSyccq8EQMtGCxmAAAAAAbo2SNUbgsZSJIo1RvEkklgieCIIk8GWJR1U0ddNHFCR0wkWJyd1OKJ1hoS1in5o44VDohVLKQeDE9iUJawS6rw/Q5qvZWD9mc49MmiyhWOiFYnR7oo4nmavZivH2Jxl0d4/mQPCY6lop2XGMrr5JntY1TppyT1IxHkVVcuR4el2jxdP2lLLhOH/R10O2lVe3TpTXCzlTfrdHs/s9OWqRBX2Lh6ntU4Pq4opJ45nRGFq2fmUlDthSlbfp1I83HdnFfJ39C1w216E0nCpHPhLwy+UszixPY3DyvuqUHzjJ/RlRiOxtaP+3UTXBSuvVGUmmbRsujusnsYzT4iSPAy2VtClnGMn1py/KzMx27jKeU1NJa78f7tf3MZV52OmHGqPpxaPaziSUqJ5PD9sX9+EX5Xi/7lvhe1mGdt5Sj5bskvVP0OK2izuPQq4+h/qPTYagW+Gw5RYDb2DlpWjH+O8PWSSPR4TEQmrwlGa5xkpL0MocO86orbxCfosleHRyYigjunUSXlr0PnHa3tdKrJ4XBXk5eGdaOsuag+EfxceGWb7Y8Mjglxbhqzm7V7fjGTw+G8VRvdlOOe6/djzfXh9K/Y+wN397Wzm81F5qP+Sy2D2fjRW/UtOo9Xwj0RZYiaRdwUF1Y+0zjN2y69nLZFViopZFXXR34qZWVpkRhgiy3rM4q0TiqQO2rI5Js2SOWTOWUCJo6JkMi5kRtGDdo0JKgAAEqMo0RugWNkSRZEmbpkgliyWEiBM2jIZJOuEiaFQ4oyJIyJySd0ah0QqlbGZLGYyCyhVJ4VirjUJoVB1iyLaFY66VYpYVDqp1S8TSKL2nXJ41ylhWJo1yGsmqlguI1zeNYp+/No4g55Q1NozLqNRG0owks0n5op44g6I4kpKOhdSRnEbDws/apQb5pJP5oqcT2JwsvYc4PpLeXqWv2k3jiiqbRnKEZbo8jiOxNSN3Sqp8k04v5oraux8dSe8k2196EldfHJn0H7UJVkzSM+855cOuR8+dbaFT91J13F5NTlUcLdbux6rYWx4UI70vFUlnKb18l0LKUo9CCpXN8aGSq1yyetXK3FVjWtXK3EVzNxNZS0I8RUK+tIkq1DjqTGDBsjqSOebN5yIZMFcmsmRs2kzRkkGrNWbM1ZJUAAA3RsjQyCTZM2TNUzKYJN0zdSIUzZMAlUjdSIEzZMEnQpkkZnMpGVIgk64zJYVDijIljMIud8JnTCoVsZk0KpoiyZZwqksapWRqkkapoGyx74KscPemO9KyiSpFnCuSrEFTGqb98ZyReMiz+0GftBV98ZVY52jaLLT7QbrEFSqxvGsTHcS2LSVY5qlU5u9IqlU9CENDlbM16xW1qxviKhX1KhhYiGzapUIJTNZTI5SMjMzKRFJhs1bJKmGaMyzDADNQCSoAABkAAkyZuAAjNzNwCCTZMymAAZTMpgAk2UjeLMgFkSRmSRqAFkTkkjM3VQyDTJJnvR3gAZBsqht3pgFGWRnvDKqGAYM3TMqoSRqACO5aT0JO8IqlQA9OvY457nDXmccpgHNZuRkicjVsAxINWzVsAEGrLbZ3Z/EV4d5TjHccnFNySba1yABDOl9j8X7sPhK/0Rj9kMX7sP6v8AABJBn9kMX7sP6v8AHQz+x+M92H9aMAA//9k='
    },
    {
      id: '3',
      pname: 'Mac-mini',
      prize: '$200',
      imageurl: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBAPEBAPDw4PDQ8NDw8PDw8NDQ0PFREWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDQ0OFg8QFSsZFRkrKy0rKystLS0tKy0uLSstLS0rKy0tLS0rNy0rKzcrLS03LS0tLS0rKysrNystNysrK//AABEIAKgBLAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAAEDBAUGBwj/xAA/EAABAwIDBAgCBwgBBQAAAAABAAIDBBEFEjEGIVFxEyIyQWGBkaFCsRQjUnKCwdEHFTNDYnOS8MJTg6Ky4f/EABkBAQEBAQEBAAAAAAAAAAAAAAABAgMEBf/EACERAQEBAAICAQUBAAAAAAAAAAABEQISAyEEBSIxQVET/9oADAMBAAIRAxEAPwD0wlASnJQkrLRikmKSsZGEYUYRhVBhEEARBVDrFx5t2O5LaWTjI6juS1Ery/Cm5axv91ez4eer5LxiB1qxv90fNey4Z2ByWOTXjZmLjf5oaMaclLiw3+aCl7l8v51vR7fB+VtWoZFXspGsI0XH6byu2fpv5EmaHEZMrb20XJ4ljbmxvGUjxsuprGXG/S+9QT0MTo3NIG8HfuX2deHlHz1jGJF1SXdxcLq9iTbw5rW6twVY2mwhv0olgGXqk20zd6DF3tZDbvy2srqSMrCczxu0A3q5VveNxHdrxWVs/O5pNhcE6LbrzcXItuVGYEL3JiVGSqyNki0aOlc8grKC38ErBodUHR4fHlaArFX2ChgdcXCKs7BQcfVnroxKoq3tqPMolmrzJUnyqtG5O9GcTxSJSm5VeIqUlEqRrkxddM47lWMm9KjSiVpoCz4Hq4HpFez3TEqXoUuiWHoQpKbok/RKsogjCMRogxUCEQRBqeyqYFZmLRktIA7lrAJnwgq6ljyL9yz/AEkPydXpAfdetYUOoOSB9E3grtMywWOTXCYysWb81BTK1io1VamXzfmzeD2eH8rrVcYFTB3IZcUjZuLgOZXm+nWTnY38ifanrHdUrzTaTEZ2Oc1shDLaLra/H4yC0OFz4rhcejfK7Po3uX2q8vjvH3rLp5A4i+8u4qptFg73MuL2VumjDXNJ0BC3MWxSLorXF7WU27Eydbf24DB6AtK0cTbZqJlY0HuUNfVB4XRhjFRFTtbcqX6J4qsKgUkUhaQQp/onil9F8UHUYHW5wFrVfYK5rAGZTvPeujqnXYg4+u7fmoXFS1vbULkE8KkempY7rRjorojMYnzb1rHDvBE3DFErNvcaKAQm+i6JmFHgrtPg/gqenNwxO4K30buC6uDBRbRJ+Eb0TXphCEoyhIXN2CUyKyayoZJPZKyIZOlZPZUIIwhARWQC8o4XqOQJ4QpVitXx3uqcTSFqTBQtjC83l8fbjY7cOWVGRuXmm3UxZK3Xv716oWblwe22EGQhwF7FeP4vx/8APyS67eXyduOOCixKxBJO5ac2PNc3KNxsqMuEOB7LvRQvwzmPIr6zx+0gqWnvWXisl9CrX7vPFU6+nLUFIEpi1TNZuRtjJ0BPIErTCkSQiEz+KujD5HaRuPkrkOz9Q7SMjmgxxM9Izu8F00Gx1Q7UAeS0afYCQ9px8tyDioq17SLW1XW0sxdFc8FuU/7PGDW5KsVOyT2MIYfIoPPKntohDdaNbgdQx93RuI4gXUlPT8RbnuRLTUFKt6lpxbRQUsIC0YUTSEA4KaGnCIKRrkRKyIKxE0Ku16kY9VGhGk8b1DG9SFyDrymRJiubuEprIk1kQNk9k9k9lQ1krIrJwEDAIg1NcDUpjVMbq4ICdEmY1QyYrGNN6pS4se5qYa05GqG4GpCwqnE5ToQFlzzyO1c71speK9nWyVcbdXBZdZiMB1IPuucc0nW55phGs8fHIt5tGWppz8I9FSmfAfgHooyxA5i6dYz2qvM2D/phUJ6GjeeswjzKvSMVWSNMTstUOCYf4eZC3KXB6EaZPZcdLGqr2q4PTYcNphpk9lbjoohpl9l5F0rxo9w5OIUjMQnGksg/EUw17A2mZ3AKQQjgF5HFjlUNJn+dir0G0tWP5t+bQmGvUBEi6IcF59T7VVXeWH8J/ValPtPMdWtPqE61O0dS7D2O1aPRVKrZmGQb2N52sVUptoHHVg9VpQ43fVh9kypvGudq9jiN8biPA7wsiowieLVhI4t3r0NuKsOrSiNVC7Ueye09PMM1tx3HxRtevQKrDaaXUNv6FYtZsj3xPI8DvCGOcEiNsiOswioi1YXDi3f7LO6axsbg8DuKqNiKRTB6yoahWBUIPRihRFCVh1MmJssPH8ZMD2xhzWl7cwLu/ksWSre/WQu5O3eyDsJayNvae0eapy47CNCXcguWypwEG9JtCfhZ6lV34vK7vA5LNaFKwLSLPTvdq5x81I1QsU7UQYQuCJM4IK8gVZ7VbeFA5qCuWrEr9oIoXFhZISP6TZb5agfC06gHmAUVzFVjkrmgxR5Qe9wJXNVu1NY2/ZFj9kr0voxwHoqlZh0UjS18bXAix3BMNefUm09Q8by2/JJ2OVhNmtz8mrsItnKWPsxN8ySpnUjAC0MaARYgC1wrSWfxzWGVtTIfrImgd7swuPJXpWKaPCoY3Z2Myu8CbeieVqIznsUZarT2qItQRgKaMJg1TRtRFiBq1KYKjA1adM1ajNaNMFpQhZ8NgLkgDidwUVTtFTxbs2d32Wb/AHVrMjfYEUkrWDM9zWji4gBcRNtVUTOyQhkQN+s4i4HG53Bc5X1EjyXTSl1iblzur5LOtzi73ENsKWK4YTM7gzs+q6HYvEnVNOZXgNvK4NbrZosvD58XijgfI36xrZA3q97raXXsH7LJjJh0UhblLy52XhcqWrI618LTqFm12BQy9pjTzG/1WqkstY4qs2NA3xuc3wPWasmTZ+pabWa7xBsvS0xaOAV1MVShKcoSstOF/aRFcwO++35LhJCRoSORIXpe3sOaFh+y/wDJeczsRYjZiM7dJX+Zv81Zj2gqB8TXfeaPyVIsTdGitmLamQdqJh5OLf1V6DauP4o5G8i1w/Jcx0afo01MdtBtJSnV5b95jh8lowYrTu7M0R8M7QfQrzjo0ujV1Or1Vjwd4IPLenJXlkYc3e0lp4tJafZXIsTqW9meXzcXj3umnV6G5QuC46LaKqGrmP8AvMb/AMbKzHtRJ8UTD91zm/O6up1rpSE1liR7TRntRyN5ZXD5hWY8cpz8Zb95rh+SaZWiQgcFEyvidpLGfxC6lzX03qohe1VpGq29V5ERSlaqkoV6VVJAiqT2qItVpzCe5RSZW9t7Gc3b/Qb0EQap4mcN6gNZEOyHSHx+rZ+qhq8WDY3OfIIQCOq0BrcvEuvcnwRGxnbH23Bvhq70Cikxu26Nn4n/AKLHt3nmSfmoK+vZA1rnZnZzZgYC7MbX7v8Ad6aY0amolkAL3kh1yGg2AF7aKnM9kYzPcGtFzc+AuVFOyeWKMxFsDngOkEgJcxpboLd4J9kVdT0zY4/pbmu6ICxkO97rWJy991FUMfrpohGKePOZGk5rF1uFh5oo8KnqKNsczy2V787idWtv2bclUxDbWNvVp485G4Pf1WjkNV0WA1rp6eOV4Ac4G9twve25A2CYBDDF0d+k+szWeAbm2q9c2PAbTNAAAudw3DVeZwU15RICScmQNGmt7r07Z2JzIGtcLHebd6DbD0QKrtKlaoqS6SYJIKRKElMSgJWdaZm00WeG3ivPauisV6bWxB7C07u+65DFaQt4EcQprUcoaZL6OtF4CWRUZvQJdAtAsQ5UFHoU3Qq+WIS1BS6JLolcyJsiCp0SXRq0WJZUFTo0sitZE2VBVdGgDbaXHI2VstUZaqChmkHxv/zcpnVEn23/AORUUbVMWojGxrF3wgAOc57r2BcbADvKw/31UH+YRyAVnatv1rP7X/IqphtAJQXFzgM4jaGtL3F5Fx6+9jwVjNTCsld2pHnwzED0RR10cb2Nc5udxFmkZt50uNLc1CxmVzm3Dg1xaHDR1jqPBY1dGJZGwCMsqDNIXzFziJIyR0YDdAGgKo6PD8ZbNO+HIWkZnB24B9jvsO4b7hcfitS4zzkOPXeWuBAG4Hc3ysBfwXU4LQ1BqJamoOZ9ujaeqM4ADQ6w3AZWgBSx7LQFznyZnlzy+1yNTe27u/26g0a7DGzhgc+RrWh12sdlD7i288rjzKgqsdpKYBmYPLAGtZH1y2wta+g9VV21mc2BjGktEklnW72hpOX5ei5Kgw6SY5Yo3PI1Pwt5nQKjXr9r6iS4iaIW8e1J6ncPILDEUsz/AI5ZDze8rscL2LJGed+4atYcrB4OefyXT0FHDEMkEQd90FrPN2rkw1xeE7DyyWMp6NuuUWc/z7gvQcNwxkbGRNPVY0NDW7zbxK1KDAppbZzlZ9kdVv8A9XTUGDRRAbrnxV9RPatgVI2PeI9/2jvK6eAXF1StZXqTRTTE7QiCZOgIIkITorPIQkKYhCQubaB7VSnpWu7QWkWqNzUwczW7NRScRyWFV7GzNuYZ3N8Cbhd8WISxRXltRheJxd0cwHEWPsqT8Tnj/jUkg8WHMF66Y1DJSMdq0HyTR5RHtDTnc5zozwexw99FegrIpOxIx/3XNJXb1ezVNJ2om+gWDXfs5pX72jKfDcrqswprIJ9gamPfBUygdwLi5vpp7KhNhuKw6iOYDi2x9rJo0UiFiOxeoj/jUkjf6mG49CPzUkO0dOe0Xxng9jvmLhEaxCayhp62KTsSRv8ABr2k+insihIUbgpkBVQowpsqCMKYBUchtc362P8Atn/2WPC5zb5XObmFnZSRmHA21C3dsm9eI9xY4ehH6rBYqxVqALQp1QiWhStJIABJ4AElVGhGpQU7KbL/ABHBh+wOvKfwjTzIV2kppHm0UeT+t9nyeXc3/d6qKVTh7JWjpms6O4cOl7yO9o1PkrdJDoyCK4G4Oe3KwfdYPz9F0eGbJEnPKSSdS4kuPquoo8MiiHVaL8Sg5Kg2YklIdM4nhfcByGgXU0ODxRAWaCeJWjZJTVwwCSSSimV2l0VMBXafREThOmCdVDokydRVUoSEZTFZaAQgcFKUJCCEhCWqUhMQghypsqmITWUxdQlqbKprJZUw1BkQmMcFYypZUxdUZaCN2rB6LLrNlaWXtRt/xC6LKmyqYa4Cu/ZtTP7ILeR/W6yJtgKmL+BUSADRpJLfS9vZerZU2RUeNy4ZisPcyYDi0A+2VVX4nUx/xaRw8WlwHuPzXtjoQe5QSYfG7Vo9EHjkW0kXxRzN/C1w+atN2lpuMg/7bvyXpdTs1TSdqJh5tBWfJsJRu/lgct3yVR55iGK0c7MjzId92lsbg5p4i4WS2npr7nVT+TI2e5P5L1ZuwNGPhd/k79Vag2NpG/Bfm4qxK8vpadnwUxdwdNKT/wCLQFu0GEVMu4DI0/DE3o2+28+ZXotPg0EfZiYPGwJV1sdtBbluV1nHJ4Xsi1li+w8NSukpaKOMWa0c+9WcqeyaYFOkQlZRTXST2T2QDZOAiDUbWqoFrVbiCja1TNQGE4TBOqCToU6ggKYokyihTEIkyACE1kdk1kAWTWUlk1kAWTWUlkrII7JWR2SsgjslZHZKyKCyVkdk1kNDZKyJJEDZLKjSQBZNZGQmQNZKyKyVkApIrJWQDZKyKyVkDZU4anAUgCAQxGGpwEQCqEGowEwRBAgE4CQThA9k9kkkFdMnSUUyZJJAySSSBJWSSQKyVkkkCypsqSSBrJWTJIFZNZJJArJkkkDpJJIGKSSSBJJJIEkkkgSeySSAgjASSQEEQTJKoIJ0kkDhOkkgcJ0kkH//2Q=='
    },
    {
      id: '4',
      pname: 'Headphones',
      prize: '$70',
      imageurl: 'https://images.unsplash.com/photo-1505740420928-5e560c06d30e?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aGVhZHBob25lc3xlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80'
    },
    {
      id: '5',
      pname: 'Iphone',
      prize: '$130',
      imageurl: 'https://www.91-img.com/gallery_images_uploads/3/d/3df5ca6a9b470f715b085991144a5b76e70da975.JPG?tr=h-550,w-0,c-at_max'
    },
    {
      id: '6',
      pname: 'Keyboard',
      prize: '$13',
      imageurl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSA0ZexTrjLGEGI03Cil4X9l03Mdhr6mguitg&usqp=CAU'
    },
    {
      id: '7',
      pname: 'Mouse',
      prize: '$15',
      imageurl: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgWFRUZGBgYGRgcFRgYGRwaHBgYGhgaGRgYGBgcIS4lHB4rIRgYJjgmKzAxNTU1GiQ7QDszPy40NTEBDAwMEA8QGhISHz8pISsxPz81NDc3NDY9NTE0PzE0MTE0ND00NjQ0MTQ0Nj80MTUxND80NjY2NDc/Nj02PzQ0Mf/AABEIAKUBMQMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABAECAwUGBwj/xAA+EAACAQIDBAgEBAUEAQUAAAABAgADEQQhMQUSQVEGIjJhcYGRoQcTsdFCUsHwFGJykuEzQ7LxIxUWRILS/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAApEQEBAAIBAwQBAgcAAAAAAAAAAQIRAyExUQQSQWGhcZEFExQjMjOB/9oADAMBAAIRAxEAPwD1+IiAiIgIiICIiAiIgIiICJQkDXLxkaptGivaqoPF1+8CVE1zbdww/wDkU/7hKDb2Fvb+Ip35b4vA2USMm0KTaVUPg6/eSFYHQ38IFYiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiazbG3cPhherUAPBBmx8F/U5Tgds/Eaq91w6imv5mszfYe8D03EYhEXed1RebEKPUznMf05wlO4VmqH+QZf3NYel55FjdqVKrb1R2c82JPpfSQK2KA1Nvr6QPScb8SKh/wBOkid7Euf0E57H9NMY4J+a2mSpZL911nInEueytu9svbWWkMe058F6o+8aGdts4ivTqVXqsqobFblnJy/Exy1m+o9IMNYAMF01BHqbTnKZCgqosGzYa3PM31mBsKh/B6XEsR2dLaNJj1aiEciRIG0MPRS7pUWm5zOQKN4qcx4ics2AThvDz+8vw+Dpg9cuy8ADb14y9Dq2VLbNjaqpQ8DmVI5g8pt8JtF9adQ+KP8AYzWPtSmHC1KYejuCx3SSrXtnyFvrJqbKwdcb1FijfmpsQR4g/wCJu44Wblsv4N1u8N0rxaaVmI5N1vrN3g/iHVH+pTR+9bqftOFq7PxlLslMSvI9R/sfUyPQ2jSZipJpuMirjdIPK5nLSvY8D05wr5MWpn+YXHqJ0OGxdOoLo6uP5SDPBXVllaGMdDdGZTzUkfSB9ARPItm9PMTTsHYVF5OM/wC4ZzrtmfEDDPYVN6k3eN5fUaekDr4mLC4lKih6bq6nRlIYeomWAiIgIiICIiAiIgIiICIiAiIgIiICIkDbG1qeGpmpUawGg4seCqOJgSsRiERS7sFVcySbATznpP8AELVMMdwcXPaP9I/CO/Xwml23tqrjG3qhK0x2KYOQHNjxM0lfcVSN1bHIi2sDXYragclmfeJzJJJJPeeMiPjU/NeWPhKA0Q+btIrUEBuu8Ld9/rLJBJNR2/lHq3+JVFA0GfM5n1kf5xGtj7H7TLTqg9x5H95wM15WAJW0BFpUCVtAttKWmS0bsDHuzF8mx3lJRvzIbH21km0oVlRnpbYxSAKGR+9hYnxtlImNwTYhi72SobDq5qQABnfjlzlXWXLUI4nw5/5immuXEYnDdW904A9dD4Hh7SXT6RK3bple9Dcf2n7zFW2oRdfUAAnzvkJrKmKXhTXxNz9LCJhe/wALt02HxSVOwwJ/Kcm9Dr5TKZrP/R0cKyOFYgEDO17X1GYlox9Wid2uu8ODAgn+4ZN55z0c/pM+K9es8xzw5Zl26X7bzBbRqUW3qbujc0Yi/jbXwM7TY3xKdLLiUFQfnSyt5r2W8t2cBSdXXeRgy8bajuYcJRknldHv2x9v4fEi9GqrHih6rjxQ5+ek2c+bEdlIZSVIzBBIIPMEZgzsNhfEfEUbLXHz05k7tQDufR/Bs++B7HE1GwukuGxY/wDDUBa12pt1XXxQ6jvFx3zbwEREBERAREQEREBERAREoTaBF2nj0oU2qVG3VQXJ/QcydJ49tnaz4qoatXqoL/KS+SLzPNjxM2HxB6Q/Nr/JU3SkbsODVLce5QfUnlOIxOIZ9T5cIEzE7SAyE1tbGX5zEwmM0zylFHq3mJs5kNJuRlVS2uUCKwMtBmzVfOXrg0bhbvECHRrEd45HXyP3kum4bTzHEeUPs5vw2b2PoZGdCDYggjyIgTbSoEjUcSbgMp6xIV7ZNbUeIkwCayxuPeaSWXsttK2l1pW0ird2N2X2lbQiwU5Q0plgGFQq+HU6qD4/S8w1TY/+NVVRoFAJHezHNj7dwmxdRIrpN3lzuMxt6RnU3tK2PQbE1AhqELuszkAX6pACg8Cd7XPSdR/7dw+6UNLUZksxJ7731nD7hVg6MUcG4Zeffb6iTq3SnFABXYf1Koue8HSdeHlwwnWdfKZY2se2Ojj4d1bDubMbKCQGB/LfRh4zDhdqAncrD5bjIkiyk/zA9k+0x1q7P1ncvfiTf/qTBUpYhfl1+2o6lUdrdHBjxt3y8fFhzZXHHpfjxWOXkvHJbNz513jM9Pn+/CYHSQ1NTCELU69FjkRmB3ryPdNs9MEAjMHMHunn5OPLjyuOU1XXHKZSZY3cQFLKQykqwN1ZSQVPMEZgzuujnxKrUrJigayab4sKijv0Djxse8zjHSR3SZV9GbI2vQxSb9CorrxtkynWzqc1PcRJ0+acBjquHcVKLsjj8SnUcmGjDuNxPU+ivxLp1d2njAtJzkKoypuf5r/6Z8br3jSQehxKA3zGh0lYCIiAiIgIiICQtr4jcpM3IGTZqOk9Mth3A5GB4K5Zmd2zZ3djf+Zyf1keo6jU58hM20nIcqMr5nxOoHneaveBvukE/rKJLVz+EW8ZjaofzW9JGAbUm/dpnyldxfC1v+pbd1Gxw2zMS1nUhkOa3IzHfaKmDxC5lLjwykjY+1vkgo4LJclSuqX1y4ib/DbXoP2ag88p7uD1k48fZlhLPudf3YuNt3tyDVGXtoV5kZf4mfD44HssD3HI+uhnQY+ij4iiGAdHSp1b3UutiCba5XlMX0epvoLcuH0lyz9Ly3/H2fpd/glyn2h4aujGzHdJ03svQ6GVxlEqN5hdc8mzGQvkeHlNfidjVqfY6y8jmPtIbYt1VkbeS4Isc105HTynK+jyvXju59d1mU+Wyp1d3DIG/E5OmnaOXLhKKQRcG8i16qimiKWIHWLEW6zKN5QOQN5HRiMwbTr/ABK/39eI5emx1jfu2toJW0iUsXwYeY+0lobi4NxPA7q2lbRKyClpS0uiVVplrLLzKQI7JMFRMrag6g6fvvk60sanINUMLwVigJzy3rc7ZgyXX2Zh1TeWq5fgTlY96gZeszMkLlN4Z3C7k6pZtAqvValu7+8OK2yIGfVJzvMey9q/LG492pnQjVf8d0nPR4rl3cPLlNZi8OM2FlP4lOQP2M65895f9t3fi+EmMx7TToioYBkIZToRI7iaDAYl6d2p5i/WXUH985vsNi0rC65NxU6+XMThlhce7W2F0mF1kp1tMTiQdD0S6bYjAkIb1aHGkxzQc6THs/0nq+Gs9s2LtaliqS1qLbyNcZixVhkysDoRPnHD4VnbdGQy3idBnb17uM956DYQUsKiAEAXIvqSxJJPrIOjiIgIiICIiAmLFU95GXmJliB899L8CaVZha3WPodPe/8AdORBIsANATfvGoM9j+JuyrkOB2sifHQ+RsfKeSPSJJsSt8mHOUVZc78xKgm3Ai3rMrgWtI4sDbQ2IgZTbPUZgmWNRBN90HPhkbeUqGNuBFvUyrWz4aEmEVpVHQgo7KQerfrAG3C/cZusL0ldcqqBx+dMj5qf0mmN+45i3dKMB4db1MGnb4HadKt2GBPFDkw8VOch7bemisXRGsMr37Xkf3aci1O/eQddCPA6ylYu2TOSoNhvC58b8fEzeOdx7XSaV+aXud4Ag5KLWA5WlvzCO2Ld4zU/aXrQFrXBscr6gdx1l3WF7i63tnrb9ZLncrvLquvCt5fTdlNwbfviJHFLihtzU6eFuELWF91huNyOh8DFngbWjiwcmyPPh/iSppTMlHEMmmY5H9OUyNrK3mGlXV9NeR18ucvvKL5SUBi8KREQLCJayzJaVkEciR8ThlqCzDMdlhqPuJNYTGwgarB0jRcq2jEbrfhJzyJ4HP2kjGbPJdflgrUOYtkCeJ7u8zPUcDtHLl/idVshGqIjEWZ9PA6H0zns4efGcd488dz48y+ds2ddyuQoY8hjTrruOptc5Z/zcvHSTf4e9ySAoFyx0A53mLpLs0HaDU10Ipk+G4u8faQukeM7NNclGZA42yF/SeX2bt18Ltuei9EYvFoiqfkUmDEcXYHq73mCbclPOfQ2GphVAAtlPOfhT0d+VSDsOs3WbuY2y8gAPEHnPS5hSIiAiIgIiICIiBznTiihwzs5AVVJJPAAT56xFQF2I0JJF9fEz1f4v7TbcSgpyZrv3hMwP7t0+U8jYyiu/G9LbSloF4YeHhKhu++XqZjIlsDOW1vlpcy8k56HSwkcOZX5l78zxEDObZ3y0uZcSc7Z6WHKYQ/ffLIGVJ1vllmYRma3WuLDI3HGXHjY55WB4TGG1seGQlSdbjhmR9IB11OYsLhl18O/zlwUOtnAOufPvHKWb3EHhkP1mRT7QIr03Ts9deR1HhLqNZX7Jz/Kdf8AMkM4GZNpgr4QNmps3AjnLvfcXXkqjjeD5/zcfPnNV/EMp3agP9XH/Mzg3FxmDoZBud/iDccxKq81FGoynq+Y4eclHE93vKqfeUJkNMZwC3PD/qZ0TEP2KRHeRb3a0sxuXaJtnUSj1FGphNjV37bqvdvEn0XL3kyh0eQduox7lAX3N50/k5/MT3RqnxY4XMjPiich6Cdjhtj4Zf8AbDf1kt7ae0kV3RBcBEQDM2CAecz7LO5txOG2bUqOoKMFJG8xBAC8cz3T0LZC55DQWUcich5AXPlOWxXSGmDZAXN82PVQczc5nyE3vRDEioWc1FZrdhSLLfIWXXQNmec17sccbJ3O9S+kOGpUkaqqj5rAKXPaKjPwHDScD0X2ccXjAbXVWDHvsbKPUX8jOp+JeL3aKIDYuxHkLE/S3nOj+EvR35dIVHXrN1jfgToPIW87znM9Y2eb1as67eibLwop01UcBJcRMBERAREQEREBKMcpWDA8S+Ijl65P5Lgjubj7W9J59WSxM9h+JGxD/qoOBDW5HX7+IE8kak45Hzt7NYwIe9LxUmV8u0pHiLSwop7pQDxCYUn8YHjHyjzBgCJbaVsw4SoqQLZcHMEiUtAv3/tLlbTPID1mKUgZ1OhOtpcHmDelQ8DJVN2S+lz67p3feY8NWzvbd6266jS57LeN8pcbMLH998oEYkbzAgEHTMkaXgSa6hhYi/74SAabUzcdZeI/enjJe9KMZZbOw2myKNCqvbYMM2XIEDnfiNMxN3htkJa6Ydn5MwJB82O7NB0Uw6viqYKgqN5mHMBTYHuvaei4jFXJznp/qMccJJhN+ax7Lb1vRqKWzqvBEpjxA9kB+szrsw/jq+Spb3Yn6TO+JmP+InG+q5fi6/SNTDHwvXZlIa77/wBTkey2EV6VFUa6KAASTaxFhrvazG+Jms2zi7UyPzkKPDVvYH1nP3555Tdt/wCtaknRotr7WqIqKjlWI3nI1toB5ne9Jzu0ndmBd2fLLeYtY8ddJlxlffqE8L2HguX3PnLiQ1ri/LK+fdN8mW8qxJqNeHMveqTym42ZgFq1dxkud0ndzUngNM9TNzS6FVmNvk+729zMNOdXbNeyhnZguS7x3rDK4G9fUAek7zo18VKtEBKuHR1/MhKMB4NvBj6TTYroTWXWl6XHsDb2msqdHqicGXuYXHqLW9DIPfNhdL8Ji7CnU3XP4H6j35Dg3/1Jm/nzPh6T0z11y5jT9+M9J6L9KayAKXLp+VzcjwbUe47oHp8SJgNopVF1Nj+U6+XOS4CIiAiIgIiIEfGYRKilXFwZymP6AYd72Fp2cQPKMd8NWF/luR4Tm8f0FxCX6gbyt7rYz3uUZAdRA+Z8R0cqg503HgcvcSLU2Q6ah18Rf3Frek+mnwNM6oPSRsRsOg4sUHpA+ZjRccQfO3/K0sdiO0pHiLfWe+Y/oDh30FpzGP8Ahsy3+Wx8IHlI3T3Su5yM67H9CMQn4A3gLf8AG3vNBiNjOmqOvlf7SjX2iZGoOOIPjl/ytLXJHaUjvtlAtZZivLnqr3zGziBkV5lV5DDzIrQJO9LXfKYt6WVXgdB0RfdqO/JDb1UfrOlqYzvnEbFxW7v58BbzJJ+gk58d3y52XWviEb5sbLDjO+aI4hjwPjbL10lA7nl63/43nPS7b3+Nmh6SY87yKrWsCzeeQ+h9ZKoYOq56qsfBbe5+03eyOg9WrUVnS2mZJOndp7SzpdlcpsvYVSqRZWz0AGfmTp4WPlO+2L8OncXfqA8tfM6mem7F2BSoIAFF+Jm4C20lRwXR7oKMPU3ybgaCd2tFRwEviBY1JTqBIWJ2RSfVB6TYRA5HH9EUzKDymlPRjca6jdPt/iekSx6QOogctsvDulgw851FEm2csXDgTMRYQKxIeArliwPAyZAREQEREBERAREQEREBERAtemDqBIOJ2PRftIPSbCIHI4/oLh30W05vHfDS1yjEeE9SiB4diPh1Xv2VPeVUn1IkOp0Arj/bX0t9J75aN0coHzxV6DVx/te7/wD6mtxPRyomqOvhmPQ5+8+mDTXkJgq7Ppv2kBgfLlTZ7jj/AHAj6XHvK0NkVHaw9gSfpb3n0jX6LYR+1RHqR+syYXo5hqZulIDzJ+sDxHZ/QeuwG6lr8WuT56D2nRYH4cVT2m3fAAfSewJTUaACXQPO8H8NqYzc3m/wnQvDJ+AGdLECBQ2RRTsoPSTUpgaAS6ICIiAiIgIiICIiAlCJWIEbC4fcLHmZJiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiIH//2Q=='
    },
    {
      id: '8',
      pname: 'Bluetooth',
      prize: '$10',
      imageurl: 'https://www.pngitem.com/pimgs/m/8-80211_bluetooth-headset-one-ear-bluetooth-earphones-hd-png.png'
    },
    {
      id: '9',
      pname: 'Earphones',
      prize: '$5',
      imageurl: 'https://wallpapercave.com/wp/wp2015683.jpg'
    },
    {
      id: '10',
      pname: 'MacBook',
      prize: '$100',
      imageurl: 'https://images.unsplash.com/photo-1606248897732-2c5ffe759c04?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjV8fG1hY2Jvb2slMjBwcm98ZW58MHx8MHx8&ixlib=rb-1.2.1&w=1000&q=80'
    },
    {
      id: '11',
      pname: 'Boat bluetooth earphones',
      prize: '$50',
      imageurl: 'https://cdn.shopify.com/s/files/1/0057/8938/4802/products/6b237ddc-f894-4dfd-a474-2435f93e6611.png?v=1625046507'
    }
  ]
}

export const GlobalContext = createContext(initialState)

// eslint-disable-next-line react/prop-types
export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState)

  const removeUser = (id) => {
    console.log('id123', id)
    dispatch({
      type: 'REMOVE_USER',
      payload: id
    })
  }

  const addUser = (user) => {
    dispatch({
      type: 'ADD_USER',
      payload: user
    })
  }

  const editUser = (user) => {
    dispatch({
      type: 'EDIT_USER',
      payload: user
    })
  }

  return (
     <>
        {console.log('state users', state.users)}
        <GlobalContext.Provider value={{
          users: state.users,
          addUser,
          editUser,
          removeUser
        }}>

            {children}
        </GlobalContext.Provider>
      </>
  )
}
