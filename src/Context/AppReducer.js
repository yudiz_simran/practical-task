export default function AppReducer (state, action) {
  switch (action.type) {
    case 'ADD_USER':
      return {
        ...state,
        users: [action.payload, ...state.users]
      }

    case 'EDIT_USER':
      // eslint-disable-next-line no-case-declarations
      const updateUser = action.payload
      // eslint-disable-next-line no-case-declarations
      const updateUsers = state.users.map(user => {
        if (user.id === updateUser.id) {
          return updateUser
        }
        return user
      })
      return {
        ...state,
        users: updateUsers
      }

    case 'REMOVE_USER':
      return {
        ...state,
        users: state.users.filter((user) => action.payload !== user.id)
      }
    default:
      return state
  }
}
